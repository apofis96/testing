﻿using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace EF.UI.Menus
{
    public class MainMenu
    {
        public static async Task Menu(HttpClient client, JsonSerializerOptions options)
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string menuItems = @"1 : Task_1.GetTasksCountByUser
2 : Task_2.GetTasksByUserList
3 : Task_3.GetFinishedTasksList
4 : Task_4.GetTeamMembersList
5 : Task_5.GetUserTasksList
6 : Task_6.GetUserStatistics
7 : Task_7.GetProjectStatisticsList
a : UserMenu
b : TeamMenu
c : TaskMenu
d : ProjectMenu
0 : Exit.";
            Console.WriteLine(menuItems);
            bool loop = true;
            while (loop)
            {
                try
                {
                    Console.Write("Main menu input: ");
                    char menu = Console.ReadKey().KeyChar;
                    Console.WriteLine("\n================================");
                    switch (menu)
                    {
                        case '1':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<TaskCountDTO>>(await client.GetStringAsync("Projects/TasksCountByUser/" + MenuInputs.IdInput()), options).Take(3), options));
                            break;
                        case '2':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<ProjectTaskDTO>>(await client.GetStringAsync("Tasks/TasksByUser/" + MenuInputs.IdInput()), options).Take(3), options));
                            break;
                        case '3':
                            Console.WriteLine(warning);
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<FinishedTaskDTO>>(await client.GetStringAsync("Tasks/FinishedTasks/" + MenuInputs.IdInput()), options).Take(3), options));
                            break;
                        case '4':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<TeamMembersDTO>>(await client.GetStringAsync("Teams/TeamMembers"), options).Take(3), options));
                            break;
                        case '5':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<UserTasksDTO>>(await client.GetStringAsync("Users/UserTasks"), options).Take(3), options));
                            break;
                        case '6':
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<UserStatisticDTO>(await client.GetStringAsync("Projects/UserStatistics/" + MenuInputs.IdInput()), options), options));
                            break;
                        case '7':
                            Console.WriteLine(warning);
                            Console.WriteLine(pause);
                            Console.ReadKey();
                            Console.WriteLine(JsonSerializer.Serialize(JsonSerializer.Deserialize<IEnumerable<ProjectStatisticDTO>>(await client.GetStringAsync("Projects/ProjectStatistics"), options).Take(3), options));
                            break;
                        case 'a':
                            UserMenu.Menu(client, options).Wait();
                            break;
                        case 'b':
                            TeamMenu.Menu(client, options).Wait();
                            break;
                        case 'c':
                            TaskMenu.Menu(client, options).Wait();
                            break;
                        case 'd':
                            ProjectMenu.Menu(client, options).Wait();
                            break;
                        case '0':
                            loop = false;
                            break;
                        default:
                            Console.WriteLine(menuItems);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

    }
}
