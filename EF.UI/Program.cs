﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using EF.UI.Menus;

namespace EF.UI
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static string host = "https://localhost:5001/";
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        static void Main(string[] args)
        {
            client.BaseAddress = new Uri(host);
            Run().GetAwaiter().GetResult();
        }
        static async Task Run()
        {
            await MainMenu.Menu(client, options);
        }
            
    }
}

