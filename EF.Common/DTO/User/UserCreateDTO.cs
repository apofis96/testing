﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF.Common.DTO.User
{
    public class UserCreateDTO
    {
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        public DateTime? Birthday { get; set; }
        public int? TeamId { get; set; }

    }
}
