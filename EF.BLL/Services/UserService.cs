﻿using AutoMapper;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace EF.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ProjectTask> _taskRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public UserService(IRepository<User> userRepository, IRepository<ProjectTask> taskRepository, IMapper mapper, IExistService existService)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> GetAll()
        {
            var users = _userRepository.Get();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
        public UserDTO Get(int id)
        {
            _existService.UserIsExistOrExeption(id);
            var user = _userRepository.Get(IRepository<User>.FilterById(id)).FirstOrDefault();
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO Post(UserCreateDTO user)
        {
            if (user.TeamId.HasValue)
                _existService.TeamIsExistOrExeption(user.TeamId.Value);
            var newUser = _mapper.Map<User>(user);
            _userRepository.Create(newUser);
            return Get(newUser.Id);
        }
        public UserDTO Update(UserUpdateDTO updateUser)
        {
            if (updateUser.TeamId.HasValue)
                _existService.TeamIsExistOrExeption(updateUser.TeamId.Value);
            _existService.UserIsExistOrExeption(updateUser.Id);
            var user = _userRepository.Get(IRepository<User>.FilterById(updateUser.Id)).FirstOrDefault();
            user.FirstName = updateUser.FirstName;
            user.LastName = updateUser.LastName;
            user.Email = updateUser.Email;
            user.Birthday = updateUser.Birthday.Value;
            user.TeamId = updateUser.TeamId;
            _userRepository.Update(user);
            return Get(updateUser.Id);
        }
        public void Delete(int id)
        {
            _existService.UserIsExistOrExeption(id);
            _userRepository.Delete(id);
        }
        public IEnumerable<UserTasksDTO> GetUserTasks()
        {
            return GetAll().OrderBy(u => u.FirstName)
                .GroupJoin(_taskRepository.Get().OrderByDescending(t => t.Name.Length).Join(_userRepository.Get(),
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u;
                    return t;
                }),
                u => u.Id, t => t.PerformerId, (u, t) => new UserTasksDTO
                {
                    User = u,
                    Tasks = _mapper.Map<IEnumerable<ProjectTaskDTO>>(t)
                });
        }
        
    }
}