﻿using AutoMapper;
using EF.Common.DTO.Team;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EF.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public TeamService(IRepository<Team> teamRepository, IRepository<User> userRepository, IMapper mapper, IExistService existService)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public IEnumerable<TeamDTO> GetAll()
        {
            var teams = _teamRepository.Get();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
        public TeamDTO Get(int id)
        {
            _existService.TeamIsExistOrExeption(id);
            var team = _teamRepository.Get(IRepository<Team>.FilterById(id)).FirstOrDefault();
            return _mapper.Map<TeamDTO>(team);
        }
        public TeamDTO Post(TeamCreateDTO team)
        {
            var newTeam = _mapper.Map<Team>(team);
            _teamRepository.Create(newTeam);
            return Get(newTeam.Id);
        }
        public TeamDTO Update(TeamUpdateDTO updateTeam)
        {
            _existService.TeamIsExistOrExeption(updateTeam.Id);
            var team = _teamRepository.Get(IRepository<Team>.FilterById(updateTeam.Id)).FirstOrDefault();
            team.Name = updateTeam.Name;
            _teamRepository.Update(team);
            return Get(updateTeam.Id);
        }
        public void Delete(int id)
        {
            _existService.TeamIsExistOrExeption(id);
            _teamRepository.Delete(id);
        }
        public IEnumerable<TeamMembersDTO> GetTeamMembers()
        {
            return GetAll().Join(_userRepository.Get()
                .OrderByDescending(u => u.RegisteredAt).GroupBy(u => u.TeamId),
                t => t.Id, u => u.Key, (t, u) => new TeamMembersDTO
                {
                    Id = t.Id,
                    Name = t.Name,
                    Members = _mapper.Map<IEnumerable<UserDTO>>(u)
                }).Where(t => t.Members.All(m => (DateTime.Today.Year - m.Birthday.Year) > 10));
        }
        
    }
}
