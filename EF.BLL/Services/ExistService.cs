﻿using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Linq;

namespace EF.BLL.Services
{
    public class ExistService : IExistService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ProjectTask> _taskRepository;
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Team> _teamRepository;
        public ExistService(IRepository<User> userRepository, IRepository<ProjectTask> taskRepository, IRepository<Project> projectRepository, IRepository<Team> teamRepository)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _teamRepository = teamRepository;
        }
        public void ProjectIsExistOrExeption(int id)
        {
            if (_projectRepository.Get(IRepository<Project>.FilterById(id)).FirstOrDefault() == null)
                throw new InvalidOperationException("Project not found");
        }
        public void ProjectTaskIsExistOrExeption(int id)
        {
            if (_taskRepository.Get(IRepository<ProjectTask>.FilterById(id)).FirstOrDefault() == null)
                throw new InvalidOperationException("Task not found");
        }
        public void TeamIsExistOrExeption(int id)
        {
            if (_teamRepository.Get(IRepository<Team>.FilterById(id)).FirstOrDefault() == null)
                throw new InvalidOperationException("Team not found");
        }
        public void UserIsExistOrExeption(int id)
        {
            if (_userRepository.Get(IRepository<User>.FilterById(id)).FirstOrDefault() == null)
                throw new InvalidOperationException("User not found");
        }
    }
}
