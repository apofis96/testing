﻿using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using System.Collections.Generic;

namespace EF.BLL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO Get(int id);
        UserDTO Post(UserCreateDTO user);
        UserDTO Update(UserUpdateDTO user); 
        void Delete(int id);
        IEnumerable<UserTasksDTO> GetUserTasks();
        
    }
}
