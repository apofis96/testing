﻿namespace EF.BLL.Interfaces
{
    public interface IExistService
    {
        void UserIsExistOrExeption(int id);
        void ProjectIsExistOrExeption(int id);
        void ProjectTaskIsExistOrExeption(int id);
        void TeamIsExistOrExeption(int id);
    }
}
