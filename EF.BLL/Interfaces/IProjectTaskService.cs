﻿using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;

namespace EF.BLL.Interfaces
{
    public interface IProjectTaskService
    {
        IEnumerable<ProjectTaskDTO> GetAll();
        ProjectTaskDTO Get(int id);
        ProjectTaskDTO Post(ProjectTaskCreateDTO projectTask);
        ProjectTaskDTO Update(ProjectTaskUpdateDTO projectTask); 
        void Delete(int id);
        IEnumerable<ProjectTaskDTO> GetTasksByUser(int userId);
        IEnumerable<FinishedTaskDTO> GetFinishedTasks(int userId);
        IEnumerable<ProjectTaskDTO> GetNotFinishedTasks(int userId);
        
    }
}
