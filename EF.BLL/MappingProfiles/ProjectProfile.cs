﻿using AutoMapper;
using EF.Common.DTO.Project;
using EF.Common.DTO.QueryResults;
using EF.DAL.Models;
using System.Collections.Generic;

namespace EF.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
            CreateMap<KeyValuePair<ProjectDTO, int>, TaskCountDTO>()
            .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.Key))
            .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));
            CreateMap<ProjectCreateDTO, Project>();
        }
    }
}
