using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EF.BLL.Tests
{
    public class ProjectServiceTests
    {
        readonly ProjectService _projectService;
        readonly IRepository<User> _userRepository;
        readonly IRepository<Project> _projectRepository;
        readonly IRepository<Team> _teamRepository;
        readonly IRepository<ProjectTask> _taskRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public ProjectServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProjectTaskProfile());
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new TeamProfile());
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _userRepository = A.Fake<IRepository<User>>();
            _projectRepository = A.Fake<IRepository<Project>>();
            _teamRepository = A.Fake<IRepository<Team>>();
            _taskRepository = A.Fake<IRepository<ProjectTask>>();
            _existExistService = A.Fake<IExistService>();
            _projectService = new ProjectService(_projectRepository, _taskRepository, _userRepository, _teamRepository, _mapper, _existExistService);
        }
        [Fact]
        public void GetTasksCountByUser_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));

            Assert.Throws<InvalidOperationException>(() => _projectService.GetTasksCountByUser(1));
        }
        [Fact]
        public void GetTasksCountByUser_WhenUserExistAndTwoProjects_ThenReturnedListLength2()
        {
            List<Project> projects = new List<Project>();
            projects.Add(new Project()
            {
                Id = 1,
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                AuthorId = 1,
                TeamId = 1
            });
            projects.Add(new Project()
            {
                Id = 2,
                CreatedAt = DateTime.Now,
                Description = "Description2",
                Name = "Name2",
                AuthorId = 1,
                TeamId = 1
            });
            projects.Add(new Project()
            {
                Id = 3,
                CreatedAt = DateTime.Now,
                Description = "Description3",
                Name = "Name3",
                AuthorId = 2,
                TeamId = 1
            });
            List<User> users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            users.Add(new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 2
            });

            A.CallTo(() => _projectRepository.Get(null)).Returns(projects);
            A.CallTo(() => _userRepository.Get(null)).Returns(users);
            var result = _projectService.GetTasksCountByUser(1);
            
            Assert.Equal(2, result.Count());
        }
        [Fact]
        public void DeleteProject_WhenProjectNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.ProjectIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Project not found"));

            Assert.Throws<InvalidOperationException>(() => _projectService.Delete(1));
        }
        [Fact]
        public void DeleteProject_WhenProjectExister_ThenProjectRepositoryDeleteIsHappened()
        {
            _projectService.Delete(1);

            A.CallTo(() => _projectRepository.Delete(A<int>._)).MustHaveHappenedOnceExactly();
        }

    }
}
