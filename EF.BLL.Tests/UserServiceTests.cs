using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.Common.DTO.User;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using Xunit;

namespace EF.BLL.Tests
{
    public class UserServiceTests
    {
        readonly UserService _userService;
        readonly FakeRepository<User> _userRepository;
        readonly IRepository<ProjectTask> _taskRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public UserServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _taskRepository = A.Fake<IRepository<ProjectTask>>();
            _userRepository = A.Fake<FakeRepository<User>>(options => options.CallsBaseMethods());
            _existExistService = A.Fake<IExistService>();
            _userService = new UserService(_userRepository, _taskRepository, _mapper, _existExistService);
        }
        [Fact]
        public void AddNewUserWithNotExistTeamId_ThenThrowInvalidOperation()
        {
            var newUser = new UserCreateDTO()
            {
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                TeamId = 1
            };
            A.CallTo(() => _existExistService.TeamIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Team not found")); 

            Assert.Throws<InvalidOperationException>(() => _userService.Post(newUser));
        }
        [Fact]
        public void AddNewUserWithoutTeamId_ThenUserRepositoryCreateIsHappened()
        {
            var newUser = new UserCreateDTO()
            {
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
            };
            A.CallTo(() => _existExistService.TeamIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Team not found"));

            _userService.Post(newUser);

            A.CallTo(() => _userRepository.Create(A<User>._)).MustHaveHappenedOnceExactly();
        }
        [Fact]
        public void ChangeUserTeam_WhenUserNotExist_ThenThrowInvalidOperation()
        {
            var updateUser = new UserUpdateDTO()
            {
                Id = 1,
                TeamId = 101,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
            };
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));
            
            Assert.Throws<InvalidOperationException>(() => _userService.Update(updateUser));
        }
        [Fact]
        public void ChangeUserTeam_WhenUserAndTeamExist_ThenReturnedUserTeamChanged()
        {
            _userRepository._entities.Add(new User()
            {
                Id = 1,
                TeamId = 100,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            });
            var updateUser = new UserUpdateDTO()
            {
                Id = 1,
                TeamId = 101,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
            };

            var returnedUser = _userService.Update(updateUser);

            A.CallTo(() => _userRepository.Update(A<User>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(1, returnedUser.Id);
            Assert.Equal(101, returnedUser.TeamId);
        }
        [Fact]
        public void DeleteUser_WhenUserNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));

            Assert.Throws<InvalidOperationException>(() => _userService.Delete(1));
        }
        [Fact]
        public void DeleteUser_WhenUserExister_ThenUserRepositoryDeleteIsHappened()
        {
            _userService.Delete(1);

            A.CallTo(() => _userRepository.Delete(A<int>._)).MustHaveHappenedOnceExactly();
        }
    }
}
