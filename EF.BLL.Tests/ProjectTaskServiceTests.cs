using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.Common;
using EF.Common.DTO.ProjectTask;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EF.BLL.Tests
{
    public class ProjectTaskServiceTests
    {
        readonly ProjectTaskService _taskService;
        readonly IRepository<User> _userRepository;
        readonly FakeRepository<ProjectTask> _taskRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public ProjectTaskServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProjectTaskProfile());
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new TeamProfile());
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _userRepository = A.Fake<IRepository<User>>();
            _taskRepository = A.Fake<FakeRepository<ProjectTask>>(options => options.CallsBaseMethods());
            _existExistService = A.Fake<IExistService>();
            _taskService = new ProjectTaskService(_taskRepository, _userRepository, _mapper, _existExistService);
        }
        [Fact]
        public void MarkTaskAsFinished_WhenTaskNotExist_ThenInvalidOperationException()
        {
           var updateTask = new ProjectTaskUpdateDTO()
            {
                Id = 1,
                Name = "testName1",
                Description = "testDescription1",
                FinishedAt = DateTime.Now,
                State = TaskState.Finished,
                ProjectId = 1,
                PerformerId = 1
            };

            A.CallTo(() => _existExistService.ProjectTaskIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Task not found"));

            Assert.Throws<InvalidOperationException>(() => _taskService.Update(updateTask));
        }
        [Fact]
        public void MarkTaskAsFinished_WhenTaskExist_ThenReturnedTaskFinished()
        {

            var task = new ProjectTask()
            {
                Id = 1,
                Name = "testName1",
                Description = "testDescription1",
                FinishedAt = DateTime.Now,
                State = TaskState.Created,
                ProjectId = 1,
                PerformerId = 1,
                CreatedAt = DateTime.Now
            };
            _taskRepository._entities.Add(task);
            var updateTask = new ProjectTaskUpdateDTO()
            {
                Id = 1,
                Name = "testName1",
                Description = "testDescription1",
                FinishedAt = DateTime.Now,
                State = TaskState.Finished,
                ProjectId = 1,
                PerformerId = 1
            };

            var returnedTask = _taskService.Update(updateTask);

            A.CallTo(() => _taskRepository.Update(A<ProjectTask>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(1, returnedTask.Id);
            Assert.Equal(TaskState.Finished, returnedTask.State);

        }
        [Fact]
        public void GetTasksByUser_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));

            Assert.Throws<InvalidOperationException>(() => _taskService.GetTasksByUser(1));
        }
        [Fact]
        public void GetTasksByUser_WhenUserExistAndTwoTasks_ThenReturnedListLength2()
        {
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 1,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Canceled
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 2,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description2",
                Name = "Name3",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Canceled
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 3,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description3",
                Name = "LongName1234567891234567891345678913465891324657891324567498732132456764315643165432132456456412345678912345678913456789134658913246578913245674987321324567643156431654321324564564",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Created
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 4,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description4",
                Name = "Name4",
                PerformerId = 2,
                ProjectId = 1,
                State = TaskState.Created
            });
            List<User> users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            users.Add(new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 2
            });
            A.CallTo(() => _userRepository.Get(null)).Returns(users);

            var result = _taskService.GetTasksByUser(1);

            Assert.Equal(2, result.Count());
        }
        [Fact]
        public void GetFinishedTasks_WhenUserExistAndOneTask_ThenReturnedListLength1()
        {
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 1,
                FinishedAt = DateTime.Parse("2019-10-29T00:26:53.01"),
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 2,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description2",
                Name = "Name2",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 3,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description3",
                Name = "Name3",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Started
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 4,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description4",
                Name = "Name4",
                PerformerId = 2,
                ProjectId = 1,
                State = TaskState.Finished
            });
            List<User> users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            users.Add(new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 2
            });
            A.CallTo(() => _userRepository.Get(null)).Returns(users);

            var result = _taskService.GetFinishedTasks(1);

            Assert.Single(result);
            Assert.Equal(2, result.First().Id);
        }
        [Fact]
        public void GetFinishedTasks_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));

            Assert.Throws<InvalidOperationException>(() => _taskService.GetFinishedTasks(1));
        }
        [Fact]
        public void GetNotFinishedTasks_WhenUseExistAndOneTask_ThenInvalidOperationException()
        {
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 1,
                FinishedAt = DateTime.Parse("2019-10-29T00:26:53.01"),
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 2,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description2",
                Name = "Name2",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 3,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description3",
                Name = "Name3",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Started
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 4,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description4",
                Name = "Name4",
                PerformerId = 2,
                ProjectId = 1,
                State = TaskState.Finished
            });
            List<User> users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            users.Add(new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 2
            });
            A.CallTo(() => _userRepository.Get(null)).Returns(users);

            var result = _taskService.GetNotFinishedTasks(1);

            Assert.Single(result);
            Assert.Equal(3, result.First().Id);
        }
        [Fact]
        public void GetNotFinishedTasks_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("User not found"));

            Assert.Throws<InvalidOperationException>(() => _taskService.GetNotFinishedTasks(1));
        }


        [Fact]
        public void DeleteTask_WhenTaskNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.ProjectTaskIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Task not found"));

            Assert.Throws<InvalidOperationException>(() => _taskService.Delete(1));
        }
        [Fact]
        public void DeleteTask_WhenTaskExister_ThenTaskRepositoryDeleteIsHappened()
        {
            _taskService.Delete(1);

            A.CallTo(() => _taskRepository.Delete(A<int>._)).MustHaveHappenedOnceExactly();
        }
    }
}
