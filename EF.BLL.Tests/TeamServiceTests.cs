using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EF.BLL.Tests
{
    public class TeamServiceTests
    {
        readonly TeamService _teamService;
        readonly IRepository<Team> _teamRepository;
        readonly IRepository<User> _userRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public TeamServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProjectTaskProfile());
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new TeamProfile());
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _userRepository = A.Fake<IRepository<User>>();
            _teamRepository = A.Fake<IRepository<Team>>();
            _existExistService = A.Fake<IExistService>();
            _teamService = new TeamService(_teamRepository, _userRepository, _mapper, _existExistService);
        }
        [Fact]
        public void GetTeamMembers_WhenOneTeamWithMembers_ThenReturnedListLength1()
        {
            var teams = new List<Team>();
            teams.Add(new Team()
            {
                Id = 1,
                Name = "testTeam1",
                Description = "testDescription2",
                CreatedAt = DateTime.Now
            });
            teams.Add(new Team()
            {
                Id = 2,
                Name = "testTeam2",
                Description = "testDescription2",
                CreatedAt = DateTime.Now
            });
            teams.Add(new Team()
            {
                Id = 3,
                Name = "testTeam3",
                Description = "testDescription3",
                CreatedAt = DateTime.Now
            });
            var users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                TeamId = 1,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2008-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2018-10-29T00:26:53.01"),
            });
            users.Add(new User()
            {
                Id = 2,
                TeamId = 1,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2007-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2019-10-29T00:26:53.01"),
            });
            users.Add(new User()
            {
                Id = 3,
                TeamId = 2,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2017-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2009-10-29T00:26:53.01"),
            });
            A.CallTo(() => _userRepository.Get(null)).Returns(users);
            A.CallTo(() => _teamRepository.Get(null)).Returns(teams);

            var result = _teamService.GetTeamMembers();

            Assert.Single(result);
            Assert.Equal(2, result.First().Members.First().Id);
        }
        [Fact]
        public void DeleteTeam_WhenTeamNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.TeamIsExistOrExeption(A<int>._)).Throws(new InvalidOperationException("Team not found"));

            Assert.Throws<InvalidOperationException>(() => _teamService.Delete(1));
        }
        [Fact]
        public void DeleteTeam_WhenTeamExister_ThenTeamRepositoryDeleteIsHappened()
        {
            _teamService.Delete(1);

            A.CallTo(() => _teamRepository.Delete(A<int>._)).MustHaveHappenedOnceExactly();
        }
    }
}
