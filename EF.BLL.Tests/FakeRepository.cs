﻿using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EF.BLL.Tests
{
    public class FakeRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        public readonly List<TEntity> _entities = new List<TEntity>();
        public FakeRepository() { }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            List<TEntity> entities = new List<TEntity>();
            if (filter != null)
            {
                entities = _entities.AsQueryable().Where(filter).ToList();
            }
            else
            {
                entities = _entities.ToList();
            }
            return entities;
        }
        public virtual void Create(TEntity entity)
        {
            _entities.Add(entity);
        }
        public virtual void Update(TEntity entity)
        {
            var update = _entities.FirstOrDefault(e => e.Id == entity.Id);
            if (update != null)
            {
                Delete(update);
                Create(entity);
            }
        }
        public virtual void Delete(int id)
        {
            var delete = _entities.FirstOrDefault(e => e.Id == id);
            if (delete != null)
            {
                Delete(delete);
            }
        }
        public virtual void Delete(TEntity entity)
        {
            _entities.Remove(entity);
        }
    }
}
