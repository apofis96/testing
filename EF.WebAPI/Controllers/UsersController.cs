﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;
        public UsersController(IUserService userService, ITeamService teamService)
        {
            _userService = userService;
            _teamService = teamService;
        }
        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return _userService.GetAll();
        }
        [HttpGet("{id}", Name = "UserGet")]
        public ActionResult<UserDTO> Get(int id)
        {
            return Ok(_userService.Get(id));
        }
        [HttpPost]
        public ActionResult<UserDTO> Post([FromBody] UserCreateDTO user)
        {            
            var newUser = _userService.Post(user);
            return CreatedAtRoute("UserGet", new { newUser.Id }, newUser);

        }
        [HttpPut]
        public ActionResult<UserDTO> Put([FromBody] UserUpdateDTO user)
        {
            var updateUser = _userService.Update(user);
            return CreatedAtRoute("UserGet", new { updateUser.Id }, updateUser);

        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _userService.Delete(id);
            return NoContent();

        }
        [HttpGet("UserTasks")]
        public ActionResult<IEnumerable<UserTasksDTO>> GetUserTasks()
        {
            return Ok(_userService.GetUserTasks());
        }
    }
}
