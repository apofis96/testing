using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.Text.Json;
using EF.Common.DTO.Project;

namespace EF.WebAPI.IntegrationTests
{
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Theory]
        [InlineData("{\"name\":\"t\",\"description\":\"testWrongDescription\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrong\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrongDescription\",\"deadline\":\"2020-20T11:17:17\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrongDescription\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":a,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrongDescription\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":a}")]
        [InlineData("{\"description\":\"testWrongDescription\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrongDescription\",\"authorId\":1,\"teamId\":1}")]
        [InlineData("WrongData")]
        [InlineData("")]
        public async Task AddProject_WhenWrongRequestBody_ThanResponseCode400(string teamJson)
        {
            var httpResponse = await _client.PostAsync("/Projects", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
        [Fact]
        public async Task AddProject_WhenRequestBodyValid_ThanResponseWithCode201AndCorrespondedBody()
        {
            var teamJson = "{\"name\":\"testName\",\"description\":\"testDescription\",\"deadline\":\"2020-07-20T11:17:17\",\"authorId\":1,\"teamId\":1}";
            var httpResponse = await _client.PostAsync("/Projects", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            var newProject = JsonSerializer.Deserialize<ProjectCreateDTO>(teamJson, options);
            var createdProject = JsonSerializer.Deserialize<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync(), options);

            await _client.DeleteAsync("/Projects/" + createdProject.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(newProject.Name, createdProject.Name);
            Assert.Equal(newProject.Description, createdProject.Description);
            Assert.Equal(newProject.Deadline.Value, createdProject.Deadline);
            Assert.Equal(newProject.AuthorId, createdProject.Author.Id);
            Assert.Equal(newProject.TeamId, createdProject.Team.Id);
        }
    }
}
