﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.DAL.Context;
using System.Linq;

namespace EF.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            
            builder.ConfigureTestServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType == typeof(DbContextOptions<EFContext>));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }
                
                var serviceDescriptor = services.Where(descriptor => descriptor.ImplementationType == typeof(EFContext)).ToList();
                foreach(var ser in serviceDescriptor)
                    services.Remove(ser);
                services.AddDbContext<DbContext, TestingContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                });
                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var appDb = scopedServices.GetRequiredService<DbContext>();
                    appDb.Database.EnsureCreated();
                }
            });
        }

        

               

    // Add ApplicationDbContext using an in-memory database for testing.
   /* services.AddDbContext<ApplicationDbContext>(options =>
               {
                   options.UseInMemoryDatabase("InMemoryDbForTesting");
               });

               // Build the service provider.
               var sp = services.BuildServiceProvider();
           }*/

        /*builder.ConfigureServices(services =>
       {
           /*
           var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<DbContext>));
           //if (descriptor != null)
               services.Remove(descriptor);
           /*var descriptorr = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<EFContext>));
           if (descriptorr != null)
               services.Remove(descriptorr);*/

        // Create a new service provider.
        //var serviceProvider = new ServiceCollection().AddEntityFrameworkInMemoryDatabase().BuildServiceProvider();

        // Add a database context (AppDbContext) using an in-memory database for testing.
        /*services.AddDbContext<DbContext, TestingContext>(options =>
        {
            options.UseInMemoryDatabase("InMemoryDb");
            //options.UseInternalServiceProvider(serviceProvider);
        });
        services.AddScoped<TestingContext>();

        // Build the service provider.
        var sp = services.BuildServiceProvider();

        // Create a scope to obtain a reference to the database contexts
        using (var scope = sp.CreateScope())
        {
            var scopedServices = scope.ServiceProvider;
            var appDb = scopedServices.GetRequiredService<DbContext>();
            // Ensure the database is created.
            appDb.Database.EnsureCreated();
        }
    });*/
        
    } 
}
