﻿using EF.DAL.Interfaces;
using EF.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EF.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly DbContext _context;
        //public Repository() { }
        public Repository(DbContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> entities = _context.Set<TEntity>();
            if (filter != null)
            {
                entities = entities.Where(filter);
            }
            return entities.ToList();
        }
        public void Create(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
        }
        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            Delete(_context.Set<TEntity>().Find(id));
        }
        public void Delete(TEntity entity)
        {
            var entities = _context.Set<TEntity>();
            if (_context.Entry(entity).State == EntityState.Detached)
                entities.Attach(entity);
            entities.Remove(entity);
            _context.SaveChanges();
        }
    }
}
